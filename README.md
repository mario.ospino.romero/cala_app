## BIENVENIDO AL MANUAL DE INSTALACIÓN
Primeramente aprenderemos a instalar lo necesario para poner en funcionamiento la API desarrollada con Flask.


##### 1- Entorno virtual

En terminos de buenas prácticas, iniciaremos instalando el entorno virtual.

- Paso 1: Abrimos la terminar de comandos.

- Paso 2: Creamos el entorno virtual con el siguiente comando
`python -m venv venv`

- Paso 3: Activamos en entorno virtual
`cd .\venv\Scripts\`

 luego le damos el siguiente comando
 `.\activate`

luego que le aparezca en  la linea de comando que el entorno está activado le debe aparecer algo asi por el estilo  "(venv)"

##### 2- Instalación de paquetes pip necesarios.
En este apartado , instalaremos en el entorno virtual los paquetes necesarios para el funcionamiento de la API.

Puede que el pip esté desactualizado por lo que primeramente lo actualizaremos.

`python -m pip install --upgrade pip`

Nota: Si no lo tienes instalado dar [Clic aquí ](https://pip.pypa.io/en/stable/installation/ "Clic aquí ") para la documentación oficial de instalación


- instalación Flask 
`pip install flask`

- instalación Pandas
`pip install pandas`

- instalación Openpyxl
`pip install openpyxl`

- instalación Pandasql
`pip install pandasql`

- instalación Flask-Cors
`pip install Flask-Cors`


##### 3- Ejecución de la API
Una vez tengas clonado el repositorio , debes irte a la carpeta api
`cd api`
y luego debemos poner en funcionamiento el servidor con el siguiente comando.
`python .\app.py`

##### 4- Prueba

Si todo marcha bien , podras ver algo parecido a esto:

[![Example](https://lh3.googleusercontent.com/-ClO1lqcM5aY/YIJ-ah2TvwI/AAAAAAAClRU/bFjw_UXGULQqPWbLDeLV-y6ZRPrVUAmpACLcBGAsYHQ/image.png "Example")](https://lh3.googleusercontent.com/-ClO1lqcM5aY/YIJ-ah2TvwI/AAAAAAAClRU/bFjw_UXGULQqPWbLDeLV-y6ZRPrVUAmpACLcBGAsYHQ/image.png "Example")


y una ruta de prueba para el navegador es la siguiente:
http://127.0.0.1:5000/api/v1/orders

##### Autor: Mario Andres Ospino Romero

### End