from flask import Flask, jsonify
from routes import *
from flask_cors import CORS ,cross_origin

app = Flask(__name__)

CORS(app)
CORS(app,resources={r"/api/v1/*":{"origins":"http://lovalhost"}})
@cross_origin
@app.route('/api/v1/orders',methods=['GET'])
def getOrders():
    #return  jsonify(result_orders)
    return result_orders
@app.route('/api/v1/path',methods=['GET'])
def getPath():
    return jsonify({0:{'path':config['development'].SOURCE_PATH}})

if __name__ == '__main__':
    app.config.from_object(config['development'])
    app.run()