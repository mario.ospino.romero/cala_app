from distutils.command.config import config
from distutils.debug import DEBUG
from pickle import TRUE


class DevelopmentConfig():
    DEBUG = True,
    PORT = 5000,
    SOURCE_PATH = '../../informacion/'


config = {
    'development': DevelopmentConfig
}