from config import config
import pandas as pd
from pandasql import sqldf

#ruta de insumo
SOURCE_PATH = config['development'].SOURCE_PATH

FECHA_CONSTANTE = '25/11/2014'

#EXTRACCION DE LOS DATOS
df_pedido = pd.read_excel(SOURCE_PATH + 'ejercicio1_b1.xlsx', sheet_name='Hoja1', usecols=['numero de pedido','Tipo de pedido','cc_cliente'])
df_cliente = pd.read_csv(SOURCE_PATH + 'ejercicio1_b2.txt', sep= "\s+")


#------------------PROCESO TRANSFORMACION -----------------------------------

#-----------TABLA PEDIDOS--------------
#REMPLAZAMOS DATOS VACIOS
df_pedido = df_pedido.fillna({'Tipo de pedido':'SIN_INFO'}) 

#AGRAGAMOS _ A LOS CAMPOS QUE LES FALTABA
df_pedido.loc[df_pedido['Tipo de pedido'] == 'SIN INFO','Tipo de pedido'] = 'SIN_INFO'

#PONEMOS EN MAYUSCULA Todo
df_pedido['Tipo de pedido'] = df_pedido['Tipo de pedido'].str.upper()

#QUITAMOS LOS ESPACIOS VACIOS
df_pedido['Tipo de pedido'] = df_pedido['Tipo de pedido'].str.replace(' ','')

#HACEMOS CRUCE JOIN
df_cliente_vs_pedido = df_cliente.merge(df_pedido,left_on='CEDULA',right_on='cc_cliente',how='inner').loc[:,['NOMBRE','APELLIDO','CEDULA','NACIMIENTO','Tipo de pedido']]


#CREAMOS CAMPO nombre_completo
df_cliente_vs_pedido['nombre_completo'] = df_cliente_vs_pedido['NOMBRE'].str.lower() + ' ' +  df_cliente_vs_pedido['APELLIDO'].str.lower()
df_cliente_vs_pedido['nombre_completo'] = df_cliente_vs_pedido['nombre_completo'].str.capitalize()


#CREAMOS CAMPO EDAD
df_cliente_vs_pedido['edad'] = int(FECHA_CONSTANTE[6:]) - df_cliente_vs_pedido['NACIMIENTO'].str.slice(6).astype(int) 


#CAMBIAMOS NOMBRE Y ORDEN DE LAS COLUMNAS
df_cliente_vs_pedido.rename(columns={
    'NOMBRE':'nombre',
    'APELLIDO':'apellido',
    'CEDULA':'cedula',
    'NACIMIENTO':'nacimiento',
    'nombre_completo':'nombre_completo',
    'edad':'edad',
    'Tipo de pedido':'tipo_de_pedido'
    },
    inplace=True
    )


#CREAMOS CAMPO NUMERO DE PEDIDO

query = """
WITH cantidad_pedidos AS (
    SELECT 
          cedula,
          tipo_de_pedido,
          COUNT(tipo_de_pedido) as numero_de_pedidos
    FROM df_cliente_vs_pedido
    GROUP BY cedula ,tipo_de_pedido
)
SELECT DISTINCT
      t1.nombre,
      t1.apellido,
      t1.cedula,
      t1.nacimiento,
      t1.nombre_completo,
      t1.edad,
      t1.tipo_de_pedido,
      t2.numero_de_pedidos
FROM df_cliente_vs_pedido t1
INNER JOIN cantidad_pedidos t2 ON t2.cedula = t1.cedula and t2.tipo_de_pedido = t1.tipo_de_pedido

"""
df_cliente_vs_pedido = sqldf(query)
#------------------FIN PROCESO TRANSFORMACION -----------------------------------



#POR TEMAS PRACTICOS SE MANDA LOS DATOS EN UNA SOLA TABLA SIN NORMALIZAR 
#print("-------- frame resultante ------------")
#print(df_cliente_vs_pedido)

#variable que contiene la respuesta
result_orders = df_cliente_vs_pedido.to_json(orient='records')