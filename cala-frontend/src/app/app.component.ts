import { Component , OnInit } from '@angular/core';
import {OrdersService} from './services/orders.service';
import { PathsourceInterface } from './models/pathsource_interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
   path_source : PathsourceInterface[] = [];

  constructor( private orderservices : OrdersService){}

  ngOnInit(): void {
    this.orderservices.getPathSource()
    .subscribe(data => {
     // console.log(' veremos el path')
     // console.log(data[0].path)
      this.path_source = data;
    });
  }

}
