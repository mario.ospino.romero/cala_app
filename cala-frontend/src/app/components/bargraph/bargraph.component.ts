import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartData, ChartEvent, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

import { OrdersService } from '../../services/orders.service';
import { OrderInterface } from '../../models/order_interface';
import { MatLabel } from '@angular/material/form-field';
import { waitForAsync } from '@angular/core/testing';

export interface interfaceDataSet {
  data: number[];
  label: string;

}

@Component({
  selector: 'app-bargraph',
  templateUrl: './bargraph.component.html',
  styleUrls: ['./bargraph.component.scss']
})
export class BargraphComponent implements OnInit{
  datos: number[] = [];
  nombresorders: string[] = [];
  personas: string[] = [];
  orders:OrderInterface[]=[];

  bardata:interfaceDataSet[]=[];

  constructor(
    private orderservices : OrdersService
  ) { }

  ngOnInit(): void {
    this.orderservices.getAllOrders()
    .subscribe(data => {
    for(let d in data){
    this.personas.push(data[d].nombre)
    this.nombresorders.push(data[d].tipo_de_pedido)
    }
    const dataArr = new Set(this.personas);
    this.personas = [...dataArr];

    const dataArr2 = new Set(this.nombresorders);
    this.nombresorders = [...dataArr2];
    this.randomize();
    });}

  @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;

  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {},
      y: {
        min: 0
      }
    },
    plugins: {
      legend: {
        display: true,
      }
    }
  };
  public barChartType: ChartType = 'bar';

  public barChartData: ChartData<'bar'> = {
    labels: [],
    datasets: [
      { data: [  ], label: '' },
      { data: [  ], label: '' }
    ]
  };

  // events
  public chartClicked({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event?: ChartEvent, active?: {}[] }): void {
    console.log(event, active);
  }

  public randomize(): void {

    this.bardata = []

    this.orderservices.getAllOrders()
    .subscribe(res => {

    for(let label in this.nombresorders){

      let dataAux = []
      for(let d in res){
        if(this.nombresorders[label] == res[d].tipo_de_pedido){
          dataAux.push(res[d].numero_de_pedidos)
        }
      }

      this.bardata.push({data:dataAux,label:this.nombresorders[label]})
    }
});

    this.barChartData = {
      labels: this.personas,
      datasets: this.bardata
    }

    this.chart?.update();
  }
}
