import { Component, OnInit,AfterViewInit,ViewChild } from '@angular/core';
import { OrdersService } from '../../services/orders.service';
import { OrderInterface } from '../../models/order_interface';
//agregamos para el tablero
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-table-orders',
  templateUrl: './table-orders.component.html',
  styleUrls: ['./table-orders.component.scss']
})


export class TableOrdersComponent implements OnInit ,  AfterViewInit {
  displayedColumns: string[] = [
    'Nombre',
    'Apellido',
    'Cedula',
    'Nacimiento',
    'Nombre Completo',
    'Edad',
    'Tipo de pedido',
    'Numero de pedido'];
  dataSource: MatTableDataSource<OrderInterface>;
  orders : OrderInterface[] = [];

  constructor(
    private orderservices : OrdersService,
  ) {
    this.dataSource = new MatTableDataSource(this.orders)
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  };

  ngOnInit(): void {
    this.orderservices.getAllOrders()
    .subscribe(data => {
      //console.log(' veremos el orders')
      //console.log(data)
      this.orders=data;
      this.dataSource = new MatTableDataSource(this.orders)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }

  refrescar(){
    this.dataSource = new MatTableDataSource(this.orders)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
