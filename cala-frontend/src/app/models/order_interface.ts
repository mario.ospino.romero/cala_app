export interface OrderInterface {
  nombre: string,
  apellido: string,
  cedula: number,
  nacimiento: string,
  nombre_completo: string,
  edad: number,
  tipo_de_pedido: string,
  numero_de_pedidos: number
}
