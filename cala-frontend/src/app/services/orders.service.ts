import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {OrderInterface } from '../models/order_interface';
import {PathsourceInterface } from '../models/pathsource_interface';
@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private URL = 'http://127.0.0.1:5000/api/v1/'
  constructor(
    private http: HttpClient,

  ) { }

  getAllOrders(){
    return this.http.get<OrderInterface[]>(this.URL + 'orders')
  }

  getPathSource(){
    return this.http.get<PathsourceInterface[]>(this.URL + 'path')
  }
}
